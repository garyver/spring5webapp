package com.example.spring5webapp.Model;
import javax.persistence.Entity;

@Entity //Api de Persistencia de Java
public class Category extends ModelBase {

    private String Name;
    private String Code;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }
}
