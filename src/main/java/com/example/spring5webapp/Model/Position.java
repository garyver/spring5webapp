package com.example.spring5webapp.Model;

import javax.persistence.Entity;

@Entity
public class Position extends ModelBase {
    private String name;

    public String getName() {
        return name;
    }

/*    public Position(String name) {
        this.name = name;
    }*/

    public void setName(String name) {
        this.name = name;
    }

}
