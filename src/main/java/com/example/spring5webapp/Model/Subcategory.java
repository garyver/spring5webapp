package com.example.spring5webapp.Model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity //Quiere decir que se utilizan los conceptos de JPA para mapear una clase a una tabla relacional
public class Subcategory extends ModelBase {
    private String Name;
    private String Code;
    @OneToOne(optional = false)
    private Category category;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }
}
